﻿using System;
using System.IO;
using System.Management.Automation;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using ExtensionMethods;

namespace PoShDbToolGui
{
    public partial class MainWindow : Form
    {
        private bool _DeleteRoot;
        private bool _UseSchemaFolders;
        private bool _UseSquareBrackets;
        private bool _DropCreate;
        private bool _LogOutput;
        private PowerShell _psInstance;
        private PSDataCollection<PSObject> _psOutputCollection;
        private IAsyncResult _psResult;
        private AsyncCallback _callback;


        public MainWindow()
        {
            InitializeComponent();

            _DeleteRoot = checkBoxDeleteRoot.Checked;
            _UseSchemaFolders = checkBoxSchemaFolder.Checked;
            _UseSquareBrackets = checkBoxBrackets.Checked;
            _LogOutput = checkBoxLogging.Checked;
            _DropCreate = checkBoxDropCreate.Checked;
        }

        #region "Script Out DB"

        /// <summary>
        /// Event triggered when error occurs in powershell script
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PsError_DataAdded(object sender, DataAddedEventArgs e)
        {
            ErrorRecord err = (sender as PSDataCollection<ErrorRecord>)[e.Index];
            string errorText = err.Exception.Message;

            DialogResult result = MessageBox.Show("An error occurred when running the script: \n" + errorText, "Powershell Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            // stop script if clicked no
            this.Invoke(new MethodInvoker(delegate
            {
                AsyncCallback asyncCallback = new AsyncCallback(DisposePowershellInstance);
                _psInstance.BeginStop(DisposePowershellInstance, null);
                SetButtonsEnableProperty(true);
            }));

        }

        /// <summary>
        /// Event triggered when new object added from powershell stream
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PsOutputCollection_DataAdded(object sender, DataAddedEventArgs e)
        {
            if (_psInstance.InvocationStateInfo.State == PSInvocationState.Running)
            {
                PSObject obj = (sender as PSDataCollection<PSObject>)[e.Index];
                // runs on different thread other than UI thread so must use Invoke method
                this.Invoke(new MethodInvoker(delegate
                {
                    if (tabControl.SelectedIndex == 0)
                    {
                        textBoxOutput.AppendText(obj.ToString() + "\n");
                    }
                    else
                    {
                        textBoxExecuteOutput.AppendText(obj.ToString() + "\n");
                    }
                }));
            }
        }

        /// <summary>
        /// Event triggered when Delete Root Folder checkbox is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBoxDeleteRoot_CheckedChanged(object sender, EventArgs e)
        {
            _DeleteRoot = checkBoxDeleteRoot.Checked;
        }

        /// <summary>
        /// Event triggered when Use Schema Folders checkbox is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBoxSchemaFolder_CheckedChanged(object sender, EventArgs e)
        {
            _UseSchemaFolders = checkBoxSchemaFolder.Checked;
        }

        /// <summary>
        /// Event triggered when Use Square Brackets checkbox is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBoxBrackets_CheckedChanged(object sender, EventArgs e)
        {
            _UseSquareBrackets = checkBoxBrackets.Checked;
        }

        /// <summary>
        /// Event triggered when Log Output to File is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBoxLogging_CheckedChanged(object sender, EventArgs e)
        {
            _LogOutput = checkBoxLogging.Checked;
        }

        /// <summary>
        /// Sets the Output Location (where the scripts will be saved to)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOutputFolder_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog browserDialog = new FolderBrowserDialog())
            {
                DialogResult dialogResult = browserDialog.ShowDialog();

                if (dialogResult == DialogResult.OK) // set output folder location only if user clicks ok
                {
                    textBoxOutputFolder.Text = browserDialog.SelectedPath;
                }
            }
        }

        /// <summary>
        /// Run "ScriptOutDatabase" powershell script
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonExecute_Click(object sender, EventArgs e)
        {
            // validate required fields have data in them
            if (ValidateFields() == false)
            {
                MessageBox.Show("Cannot execute script. Missing required fields.", "Missing Fields", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // default last modified date
            if (textLastModifiedDate.Text.Length == 0)
            {
                textLastModifiedDate.Text = "01/01/1900";
            }

            // write to object list file
            if (checkBoxObjectSearch.Checked)
            {
                WriteToObjectListTextFile();
            }

            // reference: https://blogs.msdn.microsoft.com/kebab/2014/04/28/executing-powershell-scripts-from-c/

            string powershellContents;
            string powershellLocation = "./PowershellScripts/ScriptOutDatabase.ps1";

            // read in contents of powershell script
            try
            {
                //powershellContents = "Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass \n"; // make sure execution policy is set
                powershellContents = File.ReadAllText(powershellLocation);
            }
            catch
            {
                MessageBox.Show("Failed to read in " + powershellLocation, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // disable button so user cannot run script again before its complete
            SetButtonsEnableProperty(false);

            // enable cancel button
            buttonCancel.Enabled = true;

            // setup powershell instance
            _psInstance = PowerShell.Create();
            _psInstance.AddScript(powershellContents);

            // pass parameters to script
            _psInstance.AddParameter("Argument1", textBoxServer.Text);
            _psInstance.AddParameter("Argument2", textBoxDb.Text);
            _psInstance.AddParameter("Argument3", textBoxOutputFolder.Text);
            _psInstance.AddParameter("Argument4", _DeleteRoot);
            _psInstance.AddParameter("Argument5", _UseSchemaFolders);
            _psInstance.AddParameter("Argument6", _UseSquareBrackets);
            _psInstance.AddParameter("Argument7", textBoxUsername.Text);
            _psInstance.AddParameter("Argument8", textBoxPassword.Text);
            _psInstance.AddParameter("Argument9", checkBoxObjectSearch.Checked);
            _psInstance.AddParameter("Argument10", textBoxFolders.Text);
            _psInstance.AddParameter("Argument11", _LogOutput);
            _psInstance.AddParameter("Argument12", textLastModifiedDate.Text);
            _psInstance.AddParameter("DropCreate", _DropCreate);

            // prepare collection of output stream
            _psOutputCollection = new PSDataCollection<PSObject>();

            // wire up powershell data collection to event so we can output text while it is running
            _psOutputCollection.DataAdded += PsOutputCollection_DataAdded;
            _psInstance.Streams.Error.DataAdded += PsError_DataAdded;

            // clear out output window
            textBoxOutput.Text = "";

            // start powershell script
            _callback = new AsyncCallback(OnScriptComplete);
            _psResult = _psInstance.BeginInvoke<PSObject, PSObject>(null, _psOutputCollection, null, _callback, null);
        }

        /// <summary>
        /// Validate that required fields are inputted before running powershell script
        /// </summary>
        /// <returns></returns>
        private bool ValidateFields()
        {
            if (textBoxServer.Text == "")
            {
                return false;
            }
            else if (textBoxDb.Text == "")
            {
                return false;
            }
            else if (textBoxOutputFolder.Text == "")
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Callback for when powershell process is complete
        /// </summary>
        /// <param name="ar"></param>
        private void OnScriptComplete(IAsyncResult ar)
        {
            if (ar.IsCompleted && _psInstance.InvocationStateInfo.State != PSInvocationState.Stopped)
            {
                MessageBox.Show("Scripting Complete!", "Powershell Script", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _psInstance.EndInvoke(ar);
                _psInstance.Dispose();
                _psOutputCollection.Dispose();
                buttonExecute.Invoke(new MethodInvoker(delegate { SetButtonsEnableProperty(true); }));
                buttonCancel.Invoke(new MethodInvoker(delegate { buttonCancel.Enabled = false; }));
            }
        }

        /// <summary>
        /// Event triggered when "..." button clicked for scripting specific folders
        /// When clicked it opens a list of checkbox items to choose from
        /// When clicked again it will close the checkbox list and populate the textbox with the chosen items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBrowseFolders_Click(object sender, EventArgs e)
        {
            if (checkedListBoxFolders.Visible == false)
            {
                // close other list if opened
                if (checkedListBoxObjects.Visible == true) { ButtonBrowseObjects_Click(sender, e); }

                // open list of checkbox items to choose from
                checkedListBoxFolders.Visible = true;
                buttonBrowseFolders.Text = "Close";
            }
            else
            {
                // close list of checkbox items
                checkedListBoxFolders.Visible = false;
                buttonBrowseFolders.Text = "...";

                // add selected items to text box
                textBoxFolders.Text = "";
                foreach (var item in checkedListBoxFolders.CheckedItems)
                {
                    textBoxFolders.Text += item.ToString();
                    textBoxFolders.Text += ",";
                }

                textBoxFolders.Text = textBoxFolders.Text.TrimEnd(',');

                // refreshes object list if changed
                CheckBoxObjectSearch_CheckedChanged(new object(), new EventArgs());
            }
        }

        /// <summary>
        /// Get list of objects from server when checked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBoxObjectSearch_CheckedChanged(object sender, EventArgs e)
        {
            // do not run script if no server/db was specified
            if (checkBoxObjectSearch.Checked == true && (textBoxServer.Text == "" || textBoxDb.Text == ""))
            {
                MessageBox.Show("Cannot get server objects. Serve/Datbase required.", "Missing Fields", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                checkBoxObjectSearch.Checked = false;
                return;
            }

            // get objects when it is checked and the last powershell instance is complete
            if (checkBoxObjectSearch.Checked == true && (_psResult == null || _psResult.IsCompleted == true))
            {
                ExecuteGetObjectsScript();
            }
        }

        /// <summary>
        /// Force script out to drop and create instead of alter when checked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxDropCreate_CheckedChanged(object sender, EventArgs e)
        {
            _DropCreate = checkBoxDropCreate.Checked;
        }

        /// <summary>
        /// Executes powershell script to get list of objects from server
        /// </summary>
        private void ExecuteGetObjectsScript()
        {
            // reference: https://blogs.msdn.microsoft.com/kebab/2014/04/28/executing-powershell-scripts-from-c/

            string powershellContents;
            string powershellLocation = "./PowershellScripts/ScriptOutDatabase_GetObjectList.ps1";

            // read in contents of powershell script
            try
            {
                powershellContents = File.ReadAllText(powershellLocation);
            }
            catch
            {
                MessageBox.Show("Failed to read in " + powershellLocation, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // disable button so user cannot script out db while getting server objects or try to get objects again
            SetButtonsEnableProperty(false);
            checkBoxObjectSearch.Enabled = false;

            // enable cancel button
            buttonCancel.Enabled = true;

            // update title window and cursor
            this.Cursor = Cursors.WaitCursor;
            this.Text = "PoSh GUI (Getting Object List ...)";

            // setup powershell instance
            _psInstance = PowerShell.Create();
            _psInstance.AddScript(powershellContents);

            // pass parameters to script
            _psInstance.AddParameter("Argument1", textBoxServer.Text);
            _psInstance.AddParameter("Argument2", textBoxDb.Text);
            _psInstance.AddParameter("Argument3", textBoxUsername.Text);
            _psInstance.AddParameter("Argument4", textBoxPassword.Text);
            _psInstance.AddParameter("Argument5", textBoxFolders.Text);

            // prepare collection of output stream
            _psOutputCollection = new PSDataCollection<PSObject>();

            //clear out items from checklist
            checkedListBoxObjects.Items.Clear();

            // wire up powershell data collection to event so we can output text while it is running
            _psOutputCollection.DataAdded += PsOutputCollection_ObjectAdded;
            _psInstance.Streams.Error.DataAdded += PsError_DataAdded;

            // start powershell script
            _callback = new AsyncCallback(OnGetObjectsComplete);
            _psResult = _psInstance.BeginInvoke<PSObject, PSObject>(null, _psOutputCollection, null, _callback, null);
        }

        private void WriteToObjectListTextFile()
        {
            string fileLocation = "./PowershellScripts/ScriptOutDatabase_ObjectList.txt";
            StringBuilder contents = new StringBuilder();

            try
            {
                string objectName;

                string pattern = "].[";
                string schemaPattern = "\\[";
                int patternStartIndex;
                int patternLength;

                foreach (string sqlObject in checkedListBoxObjects.CheckedItems)
                {
                    // extract just the object name
                    // e.g.. "View\[SchemaName].[ObjectName]"  --- becomes --> "ObjectName" 
                    // e.g.. "Schema\[SchemaName]              --- becomes --> "SchemaName"

                    patternStartIndex = sqlObject.IndexOf(pattern);
                    patternLength = pattern.Length;

                    if (patternStartIndex == -1)
                    {
                        // possibly scripting out schema, user, or db role... which does not have the pattern ].[
                        patternStartIndex = sqlObject.IndexOf(schemaPattern);
                        patternLength = schemaPattern.Length;
                    }

                    if (patternStartIndex >= 0)
                    {
                        objectName = sqlObject.Substring(patternStartIndex + patternLength).Replace("]", "");
                        contents.AppendLine(objectName);
                    }
                    else
                    {
                        contents.AppendLine(sqlObject);
                    }
                }

                File.WriteAllText(fileLocation, contents.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Failed to write to {0}: {1}", fileLocation, ex.ToString()), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        /// <summary>
        /// Callback for when script to get objects is complete
        /// </summary>
        /// <param name="ar"></param>
        private void OnGetObjectsComplete(IAsyncResult ar)
        {
            if (ar.IsCompleted && _psInstance.InvocationStateInfo.State != PSInvocationState.Stopped)
            {
                _psInstance.EndInvoke(ar);
                _psInstance.Dispose();
                _psOutputCollection.Dispose();

                this.Invoke(new MethodInvoker(delegate
                {
                    // update title window and cursor
                    this.Cursor = Cursors.Arrow;
                    this.Text = "PoSh GUI";
                    // open list of objects
                    if (checkedListBoxObjects.Visible == false)
                    {
                        ButtonBrowseObjects_Click(new object(), new EventArgs());
                    }

                    // re-enable buttons
                    buttonExecute.Enabled = true;
                    checkBoxObjectSearch.Enabled = true;

                    // disable cancel
                    buttonCancel.Enabled = false;
                }));
            }
        }

        /// <summary>
        /// Event triggered when new object added from powershell output of ExecuteGetObjectsScript
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PsOutputCollection_ObjectAdded(object sender, DataAddedEventArgs e)
        {
            if (_psInstance.InvocationStateInfo.State == PSInvocationState.Running)
            {
                PSObject obj = (sender as PSDataCollection<PSObject>)[e.Index];
                // runs on different thread other than UI thread so must use Invoke method
                checkedListBoxObjects.Invoke(new MethodInvoker(delegate { checkedListBoxObjects.Items.Add(obj.BaseObject.ToString()); }));
            }
        }

        /// <summary>
        /// Event triggered when button to browse objects is clicked
        /// "..."   -> opens checkbox list when first clicked
        /// "Close" -> closes checkbox list and adds selected items to Specific Objects textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBrowseObjects_Click(object sender, EventArgs e)
        {
            if (checkedListBoxObjects.Visible == false)
            {
                // close other list if opened
                if (checkedListBoxFolders.Visible == true) { ButtonBrowseFolders_Click(sender, e); }

                // open list of checkbox items to choose from
                checkedListBoxObjects.Visible = true;
                buttonBrowseObjects.Text = "Close";
            }
            else
            {
                // close list of checkbox items
                checkedListBoxObjects.Visible = false;
                buttonBrowseObjects.Text = "...";

                // add selected items to text box
                textBoxObjects.Text = "";
                foreach (var item in checkedListBoxObjects.CheckedItems)
                {
                    textBoxObjects.Text += item.ToString();
                    textBoxObjects.Text += ",";
                }

                textBoxObjects.Text = textBoxObjects.Text.TrimEnd(',');
            }
        }

        /// <summary>
        /// When window is closing, dispose of powershell object if still running
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            ButtonCancel_Click(sender, new EventArgs());
        }

        /// <summary>
        /// callback used to dispose of powershell instance when stopping it
        /// </summary>
        /// <param name="ar"></param>
        private void DisposePowershellInstance(IAsyncResult ar)
        {
            _psInstance.Dispose();
            _psOutputCollection.Dispose();
        }

        /// <summary>
        /// Cancels powershell instance when clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            if (_psResult != null && _psResult.IsCompleted == false)
            {
                AsyncCallback asyncCallback = new AsyncCallback(DisposePowershellInstance);
                _psInstance.BeginStop(DisposePowershellInstance, null);
                buttonCancel.Enabled = false;
                buttonExecute.Enabled = true;
                checkBoxObjectSearch.Enabled = true;
                this.Cursor = Cursors.Arrow;
                this.Text = "PoSH GUI";

                textBoxOutput.AppendText("\n Script Stopped ...");
            }
        }

        #endregion

        #region "Execute SQL Changes"


        /// <summary>
        /// Sets the Git Repo folder location 
        /// </summary>
        private void buttonBrowseRepo_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog browserDialog = new FolderBrowserDialog())
            {
                DialogResult dialogResult = browserDialog.ShowDialog();

                if (dialogResult == DialogResult.OK) // set output folder location only if user clicks ok
                {
                    textBoxRepoFolder.Text = browserDialog.SelectedPath;
                }
            }
        }

        private void buttonGetAffectedFiles_Click(object sender, EventArgs e)
        {

            // update title window and cursor
            this.Text = "PoSh GUI (Getting Affected Files ...)";
            SetButtonsEnableProperty(false);

            GetListOfAffectedFilesFromGit(textBoxRepoFolder.Text, textBoxGitCommit1.Text, textBoxGitCommit2.Text);

        }

        private void GetListOfAffectedFilesFromGit(string repoFolderPath, string commit1, string commit2)
        {
            // reference: https://blogs.msdn.microsoft.com/kebab/2014/04/28/executing-powershell-scripts-from-c/

            string powershellContents = "cd " + repoFolderPath;
            powershellContents += String.Format("\n git diff --name-only {0} {1}", commit1, commit2);


            // setup powershell instance
            _psInstance = PowerShell.Create();
            _psInstance.AddScript(powershellContents);

            // prepare collection of output stream
            _psOutputCollection = new PSDataCollection<PSObject>();

            //clear out items from checklist
            checkedListBoxAffectedFiles.Items.Clear();

            // wire up powershell data collection to event so we can output text while it is running
            _psOutputCollection.DataAdded += AffectedFileAdded;
            _psInstance.Streams.Error.DataAdded += GetAffectedFileErrorAdded;

            // start powershell script
            _callback = new AsyncCallback(OnGetAffectedFilesComplete);
            _psResult = _psInstance.BeginInvoke<PSObject, PSObject>(null, _psOutputCollection, null, _callback, null);

        }

        private void OnGetAffectedFilesComplete(IAsyncResult ar)
        {
            if (ar.IsCompleted && _psInstance.InvocationStateInfo.State != PSInvocationState.Stopped)
            {
                _psInstance.EndInvoke(ar);
                _psInstance.Dispose();
                _psOutputCollection.Dispose();

                this.Invoke(new MethodInvoker(delegate
                {
                    // update title window
                    this.Text = "PoSh GUI";

                    // re-enable buttons
                    SetButtonsEnableProperty(true);
                }));
            }
        }

        private void GetAffectedFileErrorAdded(object sender, DataAddedEventArgs e)
        {
            ErrorRecord err = (sender as PSDataCollection<ErrorRecord>)[e.Index];
            string errorText = err.Exception.Message;

            MessageBox.Show("An error occurred when getting files: \n" + errorText, "Powershell Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            this.Invoke(new MethodInvoker(delegate
            {
                SetButtonsEnableProperty(true);
            }));
        }

        private void AffectedFileAdded(object sender, DataAddedEventArgs e)
        {
            if (_psInstance.InvocationStateInfo.State == PSInvocationState.Running)
            {
                PSObject obj = (sender as PSDataCollection<PSObject>)[e.Index];
                // runs on different thread other than UI thread so must use Invoke method
                checkedListBoxAffectedFiles.Invoke(new MethodInvoker(delegate { checkedListBoxAffectedFiles.Items.Add(obj.BaseObject.ToString()); }));
            }
        }

        private void buttonExecuteSqlChanges_Click(object sender, EventArgs e)
        {
            if (checkedListBoxAffectedFiles.CheckedItems.Count == 0) { return; }

            // validate required fields have data in them
            if (ValidateFieldsForExecutingChanges() == false)
            {
                MessageBox.Show("Cannot execute script. Missing required fields.", "Missing Fields", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            textBoxExecuteOutput.Clear();
            WriteToPayloadTextFile();


            // reference: https://blogs.msdn.microsoft.com/kebab/2014/04/28/executing-powershell-scripts-from-c/

            string powershellContents;
            string powershellLocation = "./PowershellScripts/ExecuteDBUpdate.ps1";
            string payloadFilePath = "./PowershellScripts/DBUpdate_payload.txt";

            // read in contents of powershell script
            try
            {
                //powershellContents = "Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass \n"; // make sure execution policy is set
                powershellContents = File.ReadAllText(powershellLocation);
            }
            catch
            {
                MessageBox.Show("Failed to read in " + powershellLocation, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // disable button so user cannot run script again before its complete
            SetButtonsEnableProperty(false);

            // setup powershell instance
            _psInstance = PowerShell.Create();
            _psInstance.AddScript(powershellContents);

            // pass parameters to script
            _psInstance.AddParameter("gitRepoDir", textBoxRepoFolder.Text);
            _psInstance.AddParameter("payloadPath", payloadFilePath);
            _psInstance.AddParameter("sqlinstanceName", textBoxServerName.Text);
            _psInstance.AddParameter("databaseNames", textBoxDatabaseNames.Text);
            _psInstance.AddParameter("username", textBoxUser.Text);
            _psInstance.AddParameter("password", textBoxPwd.Text);

            // prepare collection of output stream
            _psOutputCollection = new PSDataCollection<PSObject>();

            // wire up powershell data collection to event so we can output text while it is running
            _psOutputCollection.DataAdded += PsOutputCollection_DataAdded;
            _psInstance.Streams.Error.DataAdded += PsError_DataAdded;

            // clear out output window
            textBoxOutput.Text = "";

            // start powershell script
            _callback = new AsyncCallback(OnScriptComplete);
            _psResult = _psInstance.BeginInvoke<PSObject, PSObject>(null, _psOutputCollection, null, _callback, null);

        }

        private bool ValidateFieldsForExecutingChanges()
        {
            if (textBoxServerName.Text == "")
            {
                return false;
            }

            if (textBoxRepoFolder.Text == "")
            {
                return false;
            }

            return true;
        }

        private void WriteToPayloadTextFile()
        {
            string fileLocation = "./PowershellScripts/DBUpdate_payload.txt";
            StringBuilder contents = new StringBuilder();

            try
            {
                foreach (string gitFile in checkedListBoxAffectedFiles.CheckedItems)
                {
                    contents.AppendLine(gitFile);
                }

                File.WriteAllText(fileLocation, contents.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Failed to write to {0}: {1}", fileLocation, ex.ToString()), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        #endregion

        private void SetButtonsEnableProperty(bool isEnabled)
        {
            buttonGetAffectedFiles.Enabled = isEnabled;
            buttonExecuteSqlChanges.Enabled = isEnabled;
            buttonExecute.Enabled = isEnabled;

            if (isEnabled)
            {
                Cursor = Cursors.Arrow;
            }
            else
            {
                Cursor = Cursors.WaitCursor;
            }
        }

        #region "Save / Load"

        private void tsmiSaveProfile_Click(object sender, EventArgs e)
        {
            string saveFilePath;

            // open dialog to choose where to save
            using (SaveFileDialog fileDialog = new SaveFileDialog())
            {
                fileDialog.AddExtension = true;
                fileDialog.Filter = "(*.xml)|*.xml";
                fileDialog.DefaultExt = "xml";

                DialogResult result = fileDialog.ShowDialog();

                if (result == DialogResult.Cancel) { return; }

                saveFilePath = fileDialog.FileName;
            }

            // create xml document
            XElement objectTypes = new XElement("ObjectTypes");

            foreach (object item in checkedListBoxFolders.CheckedItems)
            {
                objectTypes.Add(new XElement(item.ToString()));
            }

            XElement sqlObjects = new XElement("Objects");
            foreach (string item in textBoxObjects.Text.Split(','))
            {
                sqlObjects.Add(new XElement("Name", item));
            }

            XDocument document = new XDocument(new XElement("Root",
                                                        new XElement("ScriptOutSettings",
                                                            new XElement("Server", textBoxServer.Text),
                                                            new XElement("Database", textBoxDb.Text),
                                                            new XElement("OutputFolder", textBoxOutputFolder.Text),
                                                            new XElement("UserName", textBoxUsername.Text),
                                                            new XElement("LastModified", textLastModifiedDate.Text),
                                                            objectTypes,
                                                            sqlObjects,
                                                            new XElement("DeleteRoot", checkBoxDeleteRoot.Checked),
                                                            new XElement("UseSchemaFolders", checkBoxSchemaFolder.Checked),
                                                            new XElement("UseSquareBrackets", checkBoxBrackets.Checked),
                                                            new XElement("LogOutput", checkBoxLogging.Checked),
                                                            new XElement("DoObjSearch", checkBoxObjectSearch.Checked),
                                                            new XElement("DropCreate", checkBoxDropCreate.Checked)),
                                                         new XElement("ExecuteSqlSettings",
                                                            new XElement("Server", textBoxServerName.Text),
                                                            new XElement("DatabaseNames", textBoxDatabaseNames.Text),
                                                            new XElement("UserName", textBoxUser.Text),
                                                            new XElement("RepoFolder", textBoxRepoFolder.Text),
                                                            new XElement("Commit1", textBoxGitCommit1.Text),
                                                            new XElement("Commit2", textBoxGitCommit2.Text)
                                                            )));


            // write xml to file
            document.Save(saveFilePath);
        }

        private void tsmiLoadProfile_Click(object sender, EventArgs e)
        {
            // open dialog to choose file
            string filePath;

            // open dialog to choose where to save
            using (OpenFileDialog fileDialog = new OpenFileDialog())
            {
                fileDialog.AddExtension = true;
                fileDialog.Filter = "(*.xml)|*.xml";
                fileDialog.DefaultExt = "xml";

                DialogResult result = fileDialog.ShowDialog();

                if (result == DialogResult.Cancel) { return; }

                filePath = fileDialog.FileName;
            }

            // parse xml doc and populate objects
            XDocument document = XDocument.Load(filePath);

            XElement root = document.Element("Root");

            XElement scriptOutSettings = root.Element("ScriptOutSettings");
            XElement executeSqlSettings = root.Element("ExecuteSqlSettings");

            // Execute SQL Tab
            textBoxServerName.Text = executeSqlSettings.Element("Server").Value;
            textBoxDatabaseNames.Text = executeSqlSettings.Element("DatabaseNames").Value;
            textBoxUser.Text = executeSqlSettings.Element("UserName").Value;
            textBoxRepoFolder.Text = executeSqlSettings.Element("RepoFolder").Value;
            textBoxGitCommit1.Text = executeSqlSettings.Element("Commit1").Value;
            textBoxGitCommit2.Text = executeSqlSettings.Element("Commit2").Value;

            // Script Out DB Tab
            textBoxServer.Text = scriptOutSettings.Element("Server").Value;
            textBoxDb.Text = scriptOutSettings.Element("Database").Value;
            textBoxOutputFolder.Text = scriptOutSettings.Element("OutputFolder").Value;
            textBoxUsername.Text = scriptOutSettings.Element("UserName").Value;
            textLastModifiedDate.Text = scriptOutSettings.Element("LastModified").Value;

            XElement objectTypes = scriptOutSettings.Element("ObjectTypes");

            textBoxFolders.Text = "";
            checkedListBoxFolders.ClearSelected();

            foreach (XElement elem in objectTypes.Elements())
            {
                int index = checkedListBoxFolders.Items.IndexOf(elem.Name.LocalName);
                if (index < 0) { continue; }

                textBoxFolders.AppendText(String.Format(",{0}", elem.Name.LocalName));
                checkedListBoxFolders.SetItemChecked(index, true);
            }

            if (textBoxFolders.Text.Length > 0)
            {
                textBoxFolders.Text = textBoxFolders.Text.Substring(1);
            }


            XElement sqlObjects = scriptOutSettings.Element("Objects");

            textBoxObjects.Text = "";
            checkedListBoxObjects.Items.Clear();

            foreach (XElement elem in sqlObjects.Elements())
            {
                textBoxObjects.AppendText(String.Format(",{0}", elem.Value));
                checkedListBoxObjects.Items.Add(elem.Value, true);
            }

            if (textBoxObjects.Text.Length > 0)
            {
                textBoxObjects.Text = textBoxObjects.Text.Substring(1);
            }

            checkBoxDeleteRoot.Checked = (scriptOutSettings.GetElementX("DeleteRoot", "false") == "true");
            checkBoxSchemaFolder.Checked = (scriptOutSettings.GetElementX("UseSchemaFolders", "false") == "true");
            checkBoxBrackets.Checked = (scriptOutSettings.GetElementX("UseSquareBrackets", "false") == "true");
            checkBoxLogging.Checked = (scriptOutSettings.GetElementX("LogOutput", "false") == "true");
            checkBoxObjectSearch.Checked = (scriptOutSettings.GetElementX("DoObjSearch", "false") == "true");
            checkBoxDropCreate.Checked = (scriptOutSettings.GetElementX("DropCreate","false") == "true");

        }

        #endregion

    }
}
