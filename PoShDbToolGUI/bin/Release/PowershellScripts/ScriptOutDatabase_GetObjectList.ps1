# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# 
#  NOTE  In order to run Powershell scripts, you need to enable them... There is a security vulnerability if web-based scripts are downloaded and executed
#  ... if you see... MyFileName.ps1 cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at http://go.microsoft.com/fwlink/?LinkID=135170.
#  ... search for PowerShell, R-Click, Run As Administrator
#  ...           Get-ExecutionPolicy   
#  ...           set-executionpolicy -scope CurrentUser -executionPolicy RemoteSigned
#  ...           Get-ExecutionPolicy   
#  ...           set-executionpolicy -scope CurrentUser -executionPolicy Restricted
#  set-executionpolicy -scope CurrentUser -executionPolicy RemoteSigned
#
#            1) Find "Windows PowerShell ISE" as a program
#            2) Run as administrator
#            3) Run from a new window, not a saved file.... file based scripts are considered dangerous, so they can't run in some configurations
#
#  # To see what version of powershell you have, run this line:
#  $PSVersionTable.PSVersion
# 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

   
# EXAMPLES: HOW TO EXECUTE THIS SCRIPT
# call this script by running command prompt as administrator or running directly in powershell
# Use one of these example commands. Command arguments are separated by spaces and kept on a single line
#
# ObjectTypesToGet is a comma delimited list of ObjectTypes. leave blank for all Object Types. valid types are: Role,Rule,StoredProcedure,Schema,User,Table,View
#
# ...Below gets object list for a server/db that uses windows authentication.
# .\ScriptOutDatabase_GetObjectList.ps1 "ServerName" "DatabaseName" "" "" "ObjectTypesToGet" "true"
# .\ScriptOutDatabase_GetObjectList.ps1 "Scotty-Test" "Interject_Reporting@C2dQhQX9" "" "" "ObjectTypesToGet" "true"
#
#
# ...Below gets object list for a server/db that uses username/password and has verbose output disabled
# .\ScriptOutDatabase_GetObjectList.ps1 "ServerName" "DatabaseName" "FakeUserName" "FakeP@ssw0rd" "ObjectTypesToGet" "false"
#
#==========================================================================
#====== Code Step 1 of 4... pull the command arguments from when this script was called, and log them in the output
#==========================================================================

# Wire up input parameters to this Powershell Script
param($Argument1,$Argument2,$Argument3,$Argument4,$Argument5,$Argument6)

if($Argument1 -eq $null)  {$Argument1 = ""} # server
if($Argument2 -eq $null)  {$Argument2 = ""} # dbname
if($Argument3 -eq $null)  {$Argument3 = ""} # username
if($Argument4 -eq $null)  {$Argument4 = ""} # password
if($Argument5 -eq $null)  {$Argument5 = ""} # Specific group of objects to search on e.g. Table, View, etc...
if($Argument6 -eq "True") {$VerbosePreference="Continue"} # Verbose output

$server              = $Argument1
$dbname              = $Argument2
$username        	 = $Argument3
$password    		 = $Argument4

# Get what group of objects to output
$ObjectArray = @()
$ObjectArray  = $Argument5 -split ","

$StoredProcedure      = $False
$View                 = $False
$Table                = $False
$Role                 = $False
$Rule                 = $False
$Schema               = $False
$UserDefinedFunction  = $False
$User                 = $False

if ($ObjectArray.count -eq 1 -and $ObjectArray[0] -eq "") 
{
    $StoredProcedure      = $True
    $View                 = $True
    $Table                = $True
    $Role                 = $True
    $Rule                 = $True
    $Schema               = $True
    $UserDefinedFunction  = $True
    $User                 = $True
}

foreach ($Object in $ObjectArray) {

    $Object = $Object.replace(" ","")

    if($Object -eq "StoredProcedure") {$StoredProcedure= $True}
    if($Object -eq "View") {$View= $True}
    if($Object -eq "Table") {$Table= $True}
    if($Object -eq "Role") {$Role= $True}
    if($Object -eq "Rule") {$Rule= $True}
    if($Object -eq "Schema") {$Schema= $True}
    if($Object -eq "UserDefinedFunction") {$UserDefinedFunction= $True}
    if($Object -eq "User") {$User= $True}
}

write-verbose "StoredProcedure    : $StoredProcedure    " 
write-verbose "View               : $View               " 
write-verbose "Table              : $Table              " 
write-verbose "Role               : $Role               " 
write-verbose "Rule               : $Rule               " 
write-verbose "Schema             : $Schema             " 
write-verbose "UserDefinedFunction: $UserDefinedFunction" 
write-verbose "User               : $User               " 

#==========================================================================
#====== Code Step 2 of 4... Resolve Connection to Database and SMO object
#==========================================================================

$isAzure = ($server.Contains(".database.windows.net"))# This would be an AZURE database
if($isAzure -eq $true){write-verbose "This is an Azure db."}

#Add a reference to the SQL SDK API 
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | out-null

#Get the server itself
if(($username.Length -gt 0) -and ($password.Length -gt 0))
{
    write-verbose "Using Connection String to connect to Database..."

    $TrustedConnectionSegment =""
    if($isAzure) 
    {
        $TrustedConnectionSegment = "Trusted_Connection=False;"
    }
    $connection = New-Object System.Data.SqlClient.SqlConnection
	$connection.ConnectionString = "Server=$server;  Database=$dbname;  User ID=$username;  Password=$password;  $TrustedConnectionSegment  Connection Timeout=30;"

    $SMOserver = New-Object ("Microsoft.SqlServer.Management.Smo.Server") $connection
}
else
{
    write-verbose "Using Windows Login to connect to Database..."
    #Uses Windows Credentials
    $SMOserver = New-Object ("Microsoft.SqlServer.Management.Smo.Server") -argumentlist $server
}

# original default used Windows Login....  $SMOserver = New-Object ('Microsoft.SqlServer.Management.Smo.Server') -argumentlist $server
if($SMOserver.InstanceName -eq $null)
{
    write-verbose "No SQL server was found with the name '$server'"
    return
}

write-verbose "Successfully opened server $server'"

#Get the database on the server
$db = $SMOserver.databases[$dbname]
if($db -eq $null)
{
    write-verbose "No database was found on the server with the name '$dbname'"
    return
}

write-verbose "Successfully opened database '$dbname'"

#==========================================================================
#====== Code Step 3 of 4... Get List of Database Objects
#==========================================================================
write-verbose "Looking up database objects"
$Objects = @() # empty array

# by setting up the defaults. The search of names will be very fast
$sp = new-object ("Microsoft.SqlServer.Management.Smo.StoredProcedure")
$vw = new-object ("Microsoft.SqlServer.Management.Smo.View")
$tb = new-object ("Microsoft.SqlServer.Management.Smo.Table")
$uf = new-object ("Microsoft.SqlServer.Management.Smo.UserDefinedFunction")
$ur = new-object ("Microsoft.SqlServer.Management.Smo.User")
$sh = new-object ("Microsoft.SqlServer.Management.Smo.Schema")

#defaults for sp
$typ = $sp.GetType()
$SMOserver.SetDefaultInitFields($typ,$false)
$SMOserver.SetDefaultInitFields($typ,"CreateDate","DateLastModified","IsSystemObject")

#$sc = $SMOserver.GetDefaultInitFields($typ) #use this to view which columns are default
#$sc

#defaults for vw
$typ = $vw.GetType()
$SMOserver.SetDefaultInitFields($typ,"CreateDate","DateLastModified","IsSystemObject")

#defaults for table
$typ = $tb.GetType()
$SMOserver.SetDefaultInitFields($typ,"CreateDate","DateLastModified","IsSystemObject")

#defaults for user defined function
$typ = $uf.GetType()
$SMOserver.SetDefaultInitFields($typ,"CreateDate","DateLastModified","IsSystemObject")

#defaults for user defined function
$typ = $ur.GetType()
$SMOserver.SetDefaultInitFields($typ,"CreateDate","DateLastModified","IsSystemObject")

#defaults for schema
$typ = $sh.GetType()
$SMOserver.SetDefaultInitFields($typ,"Name","IsSystemObject")

if($StoredProcedure)      {$Objects += $db.StoredProcedures | Where-Object {!($_.IsSystemObject)}}
if($View)                 {$Objects += $db.Views | Where-Object {!($_.IsSystemObject)}}
if($Table)                {$Objects += $db.Tables | Where-Object {!($_.IsSystemObject)}}
if($Role)                 {$Objects += $db.Roles | Where-Object {!($_.IsSystemObject)}}
if($Rule)                 {$Objects += $db.Rules | Where-Object {!($_.IsSystemObject)}}
if($Schema)               {$Objects += $db.Schemas | Where-Object {!($_.IsSystemObject)}}
if($UserDefinedFunction)  {$Objects += $db.UserDefinedFunctions | Where-Object {!($_.IsSystemObject)}}
if($User)                 {$Objects += $db.Users | Where-Object {!($_.IsSystemObject)}}

#$db.StoredProcedures  | Get-Member -membertype properties

#==========================================================================
#====== Code Step 4 of 4... Loop Over Objects
#==========================================================================
write-verbose "Looping Over Objects"    

foreach ($ScriptThis in $Objects)
{
    if ($ScriptThis.IsSystemObject -eq $true){
		write-verbose "$ScriptThis is a system obj ..."
		continue;
	}

    $GetType = [string]$ScriptThis.GetType() 
    $GetType = $GetType.replace("Microsoft.SqlServer.Management.Smo.","")

    if ($GetType -eq "StoredProcedure" -or $GetType -eq "View" -or $GetType -eq "Table" -or $GetType -eq "UserDefinedFunction") {
        $ObjectName = $GetType + "\["+$ScriptThis.Schema+"].["+$ScriptThis.Name+"]"
	}
    else
    {
        $ObjectName = $GetType +"\["+$ScriptThis.Name+"]"
    }
	
"$ObjectName" # print to standard out
} #This ends the loop 


write-verbose "Finished Looping Over Objects"
$SMOserver.ConnectionContext.Disconnect();
return;