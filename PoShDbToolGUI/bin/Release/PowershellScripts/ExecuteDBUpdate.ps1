param ([string]$gitRepoDir, [string]$payloadPath, [string]$sqlinstanceName, [string]$databaseNames, [string]$username, [string]$password)
Write-Output "The git repo root is: $gitRepoDir"
Write-Output "The Payload file path is: $payloadPath" 
Write-Output "The SQL instance Name is: $sqlinstanceName" 
Write-Output "Databases to execute on: $databaseNames"
Write-Output "The username is: $username"
Write-Output "" 

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO") | out-null


#------------------------------------------------------------
#			FUNCTIONS & METHODS
#------------------------------------------------------------

function FileExist($path){return (Test-Path -Path $path);}

function Invoke-Sqlcmd2
{
param([string]$ServerInstance, [string]$Database, [string]$path)

	Write-Output "	...Connecting to $ServerInstance ..."
	$Server = Connect-ToServer -server $ServerInstance -dbName $Database -username "" -password ""

	if ($null -eq $Server) {
		throw "Could not connect to $ServerInstance... SERVER OBJECT IS NULL"
	}

	Write-Output ("	...SQL Server Version: {0}" -f $Server.Information.Version)

	Write-Output "	...Getting Database Object $Database ..."

	$dbObj = $Server.Databases | Where-Object { $_.Name -eq $Database}

	if ($null -eq $dbObj) {
		throw "Could not find $Database... DB OBJECT IS NULL"
	}

	Write-Output "	...Running Script On Database..."
	$sr = New-Object System.IO.StreamReader($path)
	$script = $sr.ReadToEnd()
	$extype = [Microsoft.SQLServer.Management.Common.ExecutionTypes]::Default
	$dbObj.ExecuteNonQuery($script, $extype)
}

function ExecuteScript {
Param($path, $dbName, $sqlFile)
	$result = "	STATUS: SUCCESS";
	Write-Output "	EXECUTING SCRIPT ON $dbName - $sqlFile"

	try {
		Invoke-Sqlcmd2 $sqlinstanceName $dbName $path
	} catch {
		$result = "	STATUS: FAILURE | ERROR: {0}" -f $_.Exception.GetBaseException().Message
	}

	Write-Output $result
	return
}

function AddToLog($logText){
	$Logfolder = "$logPath"
	if (!(Test-Path $Logfolder)) {
		[void](new-item $Logfolder -itemType directory)
	}
	$date = (get-date).ToString('yyyyMMdd-HH-MM')
	$dateToDisplay = (get-date).ToString('MM/dd/yyyy HH:MM')
	$file = "$Logfolder\UpdateLog-$date.log"
	if(!(Test-Path -Path $file)){
		[void](New-Item -type file $file)
	}
	add-content -Path $file -Value ""
	add-content -Path $file -Value "----SQL SCRIPT EXECUTED ON: $dateToDisplay ------"
	add-content -Path $file -Value ""
	add-content -Path $file -Value $logText	
	add-content -Path $file -Value "----END------"	
}

function Connect-ToServer {
Param([string] $server, [string] $dbName, [string] $username, [string] $password)

	$isAzure = ($server.Contains(".database.windows.net"))# This would be an AZURE database
	if($isAzure -eq $true){ Write-Host "This is an Azure db."}

	#Get the server itself
	if(($username.Length -gt 0) -and ($password.Length -gt 0))
	{
		
		Write-Host "Using Connection String to connect to Database..."

		$TrustedConnectionSegment =""
		if($isAzure) 
		{
			$TrustedConnectionSegment = "Trusted_Connection=False;"
		}
		$connection = New-Object System.Data.SqlClient.SqlConnection
		$connection.ConnectionString = "Server=$server;  Database=$dbname;  User ID=$username;  Password=$password;  $TrustedConnectionSegment  Connection Timeout=30;"

		$SMOserver = New-Object ("Microsoft.SqlServer.Management.Smo.Server") $connection
	}
	else
	{
		Write-Host "Using Windows Login to connect to Database..."
		#Uses Windows Credentials
		$SMOserver = New-Object ("Microsoft.SqlServer.Management.Smo.Server") -argumentlist $server
	}

	# original default used Windows Login....  $SMOserver = New-Object ('Microsoft.SqlServer.Management.Smo.Server') -argumentlist $server
	if($null -eq $SMOserver.Information.Version)
	{
		Write-Host "No SQL server was found with the name '$server'"
		return $null
	}
	else 
	{
		Write-Host "Successfully opened server '$server'"
		return $SMOserver
	}

}


$logPath = $gitRepoDir + "\Logs\DBUpdateLog"
$dbArray = $databaseNames -split "," | Where-Object {$_}

Write-Output "The logPath is:  $logPath"
Write-Output "Database to loop through: $dbArray"

# $DataSource= $instanceName
if(!$sqlinstanceName){
	Write-Output "Server Name (instanceName) is empty string or null"
	Exit 99999;
}


if(!$payloadPath){
	Write-Output "payloadPath is empty string or null"
	Exit 99999;
}elseif($payloadPath.IndexOf(".txt") -eq -1){
	Write-Output "Not a .txt file: $payloadPath"
	Exit 99999;
}
elseif(!(FileExist($payloadPath))){
	Write-Output "File does not exist: $payloadPath"
	Exit 99999;
}



#####################################
###     SQL SCRIPT EXECUTION	  ###	
#####################################

Write-Output "`r`n-- LOOPING OVER FILES TO EXECUTE --`r`n`r`n"

$lines = get-content $payloadPath

foreach ($line in $lines) {
	if ($line -eq $null) {continue;}
	if($line.ToLower().IndexOf(".sql") -eq -1) {continue;}
	
	$dbName = $line.split('/')[0];
	$objectType = $line.split('/')[1];
	$sqlFile = $line.split('/')[2];
	$sqlFilePath = "{0}\{1}" -f $gitRepoDir, $line.replace("/", "\")

	Write-Output "Database Name: $dbName ,Object Type: $objectType ,SQL File: $sqlFile ,Full Path: $sqlFilePath"

	if(!(FileExist($sqlFilePath))){
		Write-Output "	Could not find the file $sqlFilePath ... SKIPPING."
		continue;
	}

	if ($dbArray.Count -eq 0) {
		# do execute on the same database
		ExecuteScript -path $sqlFilePath -dbName $dbName -sqlFile $sqlFile
	}
	else {
		# loop on given databases and execute on each db
		foreach ($db in $dbArray) {
			ExecuteScript -path $sqlFilePath -dbName $db -sqlFile $sqlFile
			Write-Output ""
		}
	}

	Write-Output ""
}

Write-Output "Done Executing SQL Files on Database(s)..."
Exit 0;

